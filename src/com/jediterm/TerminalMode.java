/**
 * 
 */
package com.jediterm;

public enum TerminalMode {
	Null,
	CursorKey,
	ANSI,
	WideColumn,
	SmoothScroll,
	ReverseScreen,
	RelativeOrigin,
	WrapAround,
	AutoRepeat,
	Interlace
}